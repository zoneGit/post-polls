# polls

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## Assumptions

### data.json

I add votes property to each poll. Votes represent the number of each answer that readers voted.

### API url

API will become usable if the variable(newConfig.baseURL) in request.js is updated.

### Today's poll

- User can poll directly at "Today's poll" section.
- The maximum number of answers is 6 when type is Multi.

#### Logic of rendering the pie chart

- use integer instead of float
- use Math.round to transfer float to integer
- the last option's vote ratio will be adjusted to make the sum of ratio to become 100
