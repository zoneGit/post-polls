/* eslint-disable no-undef */
import { transformTimeToDate, transformTimeToDateTime } from "@/utils/date";

describe("test transformTimeToDate", () => {
  it("0 to be 1 JAN 1970 ", () => {
    const result = transformTimeToDate(0);
    expect(result).toBe("1 JAN 1970");
  });

  it("1643904815611 to be 4 FEB 2022", () => {
    const result = transformTimeToDate(1643904815611);
    expect(result).toBe("4 FEB 2022");
  });
});

describe("test transformTimeToDateTime", () => {
  it("0 to be Thursday,1 January, 1970, 08:00 AM", () => {
    const result = transformTimeToDateTime(0);
    expect(result).toBe("Thursday,1 January, 1970, 08:00 AM");
  });

  it("1643904815611 to be Friday,4 February, 2022, 12:13 AM", () => {
    const result = transformTimeToDateTime(1643904815611);
    expect(result).toBe("Friday,4 February, 2022, 12:13 AM");
  });
});
