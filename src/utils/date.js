export const transformTimeToDate = (time) => {
  const months = [
    "JAN",
    "FEB",
    "MAR",
    "APR",
    "MAY",
    "JUN",
    "JUL",
    "AUG",
    "SEP",
    "OCT",
    "NOV",
    "DEC",
  ];
  const date = new Date(time);
  return `${date.getDate()} ${
    months[date.getMonth()]
  } ${date.getUTCFullYear()}`;
};

export const transformTimeToDateTime = (time) => {
  const date = new Date(time);
  const dateStr = date
    .toLocaleString("en-us", {
      year: "numeric",
      month: "long",
      day: "numeric",
      weekday: "long",
    })
    .replace(/[,]\s(\w+)\s(\w+)[,]/, ",$2 $1,");

  const timeStr = date.toLocaleTimeString("en-us", {
    hour: "2-digit",
    minute: "2-digit",
    hour12: true,
  });

  return `${dateStr}, ${timeStr}`;
};
