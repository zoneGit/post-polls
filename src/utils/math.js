export const transformPercentageToDegree = (ratio) => ratio * 0.01 * 360;
