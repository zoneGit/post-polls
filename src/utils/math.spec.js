/* eslint-disable no-undef */
import { transformPercentageToDegree } from "@/utils/math";

describe("test transformPercentageToDegree", () => {
  it("0% should be 0 deg", () => {
    const result = transformPercentageToDegree(0);
    expect(result).toBe(0);
  });
  it("100% should be 360 deg", () => {
    const result = transformPercentageToDegree(100);
    expect(result).toBe(360);
  });
});
