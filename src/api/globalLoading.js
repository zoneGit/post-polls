import { ElLoading } from "element-plus";

let waitingActionNum = 0;
let elLoading = null;

const add = () => {
  waitingActionNum += 1;
  if (waitingActionNum === 1) {
    elLoading = ElLoading.service({
      lock: true,
      text: "Loading",
      spinner: "el-icon-loading",
      background: "rgba(0, 0, 0, 0.5)",
    });
  }
};

const done = () => {
  if (waitingActionNum <= 0) return;
  waitingActionNum -= 1;
  if (waitingActionNum === 0) elLoading.close();
};

export default {
  add,
  done,
};
