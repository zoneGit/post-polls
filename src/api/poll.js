import request from "./request";

// TODO: replace with real resource
export function getPollList() {
  return request.get("/data.json");
}

export function getPollDetail(id) {
  return request.get("/data.json").then(({ data }) => {
    const poll = data.polls.filter((poll) => poll.id === id * 1)[0];
    return Promise.resolve({ data: poll });
  });
}

export function sendPoll(pollID, optionID) {
  return request.get(`/poll/${pollID}?id=${optionID}`);
}
