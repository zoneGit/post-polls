import axios from "axios";
import globalLoading from "./globalLoading";

axios.interceptors.request.use((config) => {
  const newConfig = Object.create(config);
  globalLoading.add();

  // TODO: replace with real API path
  newConfig.baseURL = "//localhost";

  newConfig.timeout = 0;
  return newConfig;
});

axios.interceptors.response.use(
  (res) => {
    globalLoading.done();
    return res;
  },
  (error) => {
    globalLoading.done();
    return Promise.reject(error);
  }
);

export default axios;
