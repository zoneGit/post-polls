import { createRouter, createWebHashHistory } from "vue-router";
import Home from "../views/Home.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/polls",
    name: "Poll List",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/PollList.vue"),
  },
  {
    path: "/poll/:id",
    name: "Poll Detail",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/PollDetail.vue"),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
